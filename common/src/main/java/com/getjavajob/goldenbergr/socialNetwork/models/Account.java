package com.getjavajob.goldenbergr.socialNetwork.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Account extends NetworkEntity {

    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String homeAddress;
    private String workAddress;
    private String email;
    private String password;
    private String icq;
    private String skype;
    private String additionalInfo;
    private Date registrationDate;

    private List<Phone> phoneNumbers = new ArrayList<>();
    private Set<Account> friends = new HashSet<>();
    private Set<Community> communities = new HashSet<>();;

    public Account() {}

    public Account(String firstName, String lastName, Date dateOfBirth, String homeAddress,
                   String workAddress, String email, String password, String icq, String skype, String additionalInfo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.email = email;
        this.password = password;
        this.icq = icq;
        this.skype = skype;
        this.additionalInfo = additionalInfo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void parseDateOfBirth(String date) {
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        try {
            this.dateOfBirth = f.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(lastName, account.lastName) &&
                Objects.equals(dateOfBirth, account.dateOfBirth) &&
                Objects.equals(homeAddress, account.homeAddress) &&
                Objects.equals(workAddress, account.workAddress) &&
                Objects.equals(email, account.email) &&
                Objects.equals(password, account.password) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInfo, account.additionalInfo) &&
                Objects.equals(registrationDate, account.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, dateOfBirth, homeAddress, workAddress, email, password, icq, skype, additionalInfo, registrationDate);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", registrationDate=" + registrationDate +
                '}';
    }
}
