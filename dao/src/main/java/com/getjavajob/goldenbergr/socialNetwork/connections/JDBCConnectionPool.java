package com.getjavajob.goldenbergr.socialNetwork.connections;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class JDBCConnectionPool extends AbstractConnectionPool {

    private static class InstanceHolder {
        private static JDBCConnectionPool INSTANCE = new JDBCConnectionPool();
    }

    private String url;
    private String username;
    private String password;

    private JDBCConnectionPool() {}

    public static JDBCConnectionPool getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected Connection createConnection() {
        if (url == null) {
            init();
        }
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new ConnectionPoolException("couldn't get a connection from DriverManager", e);
        }
        return connection;
    }

    private void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Properties props = new Properties();
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("dbConnection.properties")) {
            props.load(is);
        } catch (IOException e) {
            throw new ConnectionPoolException("processing properties failed", e);
        }
        url = props.getProperty("database.url");
        username = props.getProperty("database.user");
        password = props.getProperty("database.password");
    }

    //testing!
    void reset() {
        for (Connection connection : idleConnections) {
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        InstanceHolder.INSTANCE = new JDBCConnectionPool();
    }
}
