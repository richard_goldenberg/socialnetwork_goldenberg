package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.DataSourceHolder;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import org.h2.tools.RunScript;
import org.junit.*;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AccountDAOTest extends DAOTest {

    private static AccountDAO accountDao;
    private Account raw;

    @BeforeClass
    public static void init() throws SQLException {
        DataSourceHolder.setDataSource(createTestDataSource());
        accountDao = AccountDAO.getInstance();
    }

    @AfterClass
    public static void shutdown() {
        accountDao = null;
    }

    @Before
    public void setUp() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/setup_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        raw = new Account("Foo", "Bar", null, null, null, "email@mail.mail", "mYpAsSword", null, null, null);
        raw.parseDateOfBirth("12-12-1970");
    }

    @After
    public void tearDown() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/teardown_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        raw = null;
    }

    @Test
    public void testInsertAndRead() throws DAOException, ParseException {
        accountDao.insert(raw);
        Account pulled = accountDao.selectByID(raw.getId());
        assertEquals(raw, pulled);
    }

    @Test
    public void testDelete() throws DAOException {
        accountDao.insert(raw);
        accountDao.deleteByID(raw.getId());
        assertNull(accountDao.selectByID(raw.getId()));
    }

    @Test
    public void testUpdate() throws DAOException {
        accountDao.insert(raw);
        String newName = "FUBAR";
        raw.setFirstName(newName);
        accountDao.update(raw);
        Account updated = accountDao.selectByID(raw.getId());
        assertEquals(raw.getFirstName(), updated.getFirstName());
        assertEquals(raw, updated);
    }
}