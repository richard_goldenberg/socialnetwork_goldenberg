package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.ConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JNDIConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class AccountDAO extends AbstractDAO<Account> {

    private static final String TABLE = "account";
    private static final String INSERT_VALUES = "NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
    private static final String ID_TAG = "account_id";
    private static final String UPDATE_VALUES = "first_name = ?, last_name = ?, date_of_birth = ?, home_address = ?, " +
                                                "work_address = ?, email = ?, password = ?, icq = ?, skype = ?, additional_info = ?";

    protected AccountDAO() {
        super(TABLE, INSERT_VALUES, ID_TAG, UPDATE_VALUES, null);
    }

    private AccountDAO(ConnectionPool pool) {
        this();
        this.pool = pool;
    }

    private static class InstanceHolder {
        private static final AccountDAO INSTANCE = new AccountDAO(JNDIConnectionPool.getInstance());
    }

    public static AccountDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Account entity) throws SQLException {
        setCommonValues(statement, entity);

        long currentTime = System.currentTimeMillis();
        java.util.Date currentDate = new java.util.Date(currentTime);
        Timestamp stamp = new Timestamp(currentTime);

        entity.setRegistrationDate(currentDate);
        statement.setTimestamp(11, stamp);
    }

    private void setCommonValues(PreparedStatement statement, Account entity) throws SQLException {
        statement.setString(1, entity.getFirstName());
        statement.setString(2, entity.getLastName());
        statement.setDate(3, parseFromDate(entity.getDateOfBirth()));
        statement.setString(4, entity.getHomeAddress());
        statement.setString(5, entity.getWorkAddress());
        statement.setString(6, entity.getEmail());
        statement.setString(7, entity.getPassword());
        statement.setString(8, entity.getIcq());
        statement.setString(9, entity.getSkype());
        statement.setString(10, entity.getAdditionalInfo());
    }

    @Override
    protected Account readEntity(ResultSet results) throws DAOException, SQLException {
        Account account = new Account();
        account.setId(results.getLong(1));
        account.setFirstName(results.getString(2));
        account.setLastName(results.getString(3));
        account.setDateOfBirth(parseToDate(results.getDate(4)));
        account.setHomeAddress(results.getString(5));
        account.setWorkAddress(results.getString(6));
        account.setEmail(results.getString(7));
        account.setPassword(results.getString(8));
        account.setIcq(results.getString(9));
        account.setSkype(results.getString(10));
        account.setAdditionalInfo(results.getString(11));
        account.setRegistrationDate(parseTimestamp(results.getTimestamp(12)));
        return account;
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Account entity) throws SQLException {
        setCommonValues(statement, entity);
        statement.setLong(11, entity.getId());
    }
}
