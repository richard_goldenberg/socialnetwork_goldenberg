package com.getjavajob.goldenbergr.socialNetwork.servlets;

import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import com.getjavajob.goldenbergr.socialNetwork.sevice.AccountService;
import com.getjavajob.goldenbergr.socialNetwork.sevice.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ListAllAccounts extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter pw = resp.getWriter();
        List<Account> accounts = null;
        try {
            accounts = new AccountService().getAll();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        openTable(pw);
        for (Account account : accounts) {
            writeAccountToTable(account, pw);
        }
        closeTable(pw);
    }

    private void openTable(PrintWriter pw) {
        pw.write("<html><body><table>" +
                "<tr><td>LAST NAME</td><td>FIRST NAME</td><td>EMAIL</td><td>PASSWORD</td></tr>");
    }

    private void writeAccountToTable(Account account, PrintWriter pw) {
        pw.write("<tr>");
        pw.write("<td>" + account.getLastName() + "</td>");
        pw.write("<td>" + account.getFirstName() + "</td>");
        pw.write("<td>" + account.getEmail() + "</td>");
        pw.write("<td>" + account.getPassword() + "</td>");
        pw.write("</tr>");
    }

    private void closeTable(PrintWriter pw) {
        pw.write("</table></body></html>");
    }
}
