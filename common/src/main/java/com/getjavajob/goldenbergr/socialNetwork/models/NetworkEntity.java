package com.getjavajob.goldenbergr.socialNetwork.models;

public abstract class NetworkEntity {

    protected NetworkEntity() {}

    protected long id;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
