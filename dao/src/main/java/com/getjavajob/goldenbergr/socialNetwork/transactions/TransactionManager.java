package com.getjavajob.goldenbergr.socialNetwork.transactions;

import com.getjavajob.goldenbergr.socialNetwork.connections.JNDIConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManager {

    private static Connection connection = null;

    public static void startTransaction() {
        connection = JNDIConnectionPool.getInstance().getConnection();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            JNDIConnectionPool.getInstance().releaseConnection();
            throw new TransactionalException("setting autocommit to false failed, releasing connection", e);
        }
    }

    public static void commitTransaction() {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new TransactionalException("commit failed, releasing connection", e);
        } finally {
            JNDIConnectionPool.getInstance().releaseConnection();
        }
    }

    public static void rollbackTransaction() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new TransactionalException("rollback failed, releasing connection", e);
        } finally {
            JNDIConnectionPool.getInstance().getConnection();
        }
    }
}
