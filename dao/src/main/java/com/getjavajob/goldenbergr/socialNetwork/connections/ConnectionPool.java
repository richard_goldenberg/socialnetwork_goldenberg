package com.getjavajob.goldenbergr.socialNetwork.connections;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();

    void releaseConnection();
}
