package com.getjavajob.goldenbergr.socialNetwork.transactions;

public class TransactionalException extends RuntimeException {

    public TransactionalException(String message) {
        super(message);
    }

    public TransactionalException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionalException(Throwable cause) {
        super(cause);
    }
}
