package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.ConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JNDIConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import com.getjavajob.goldenbergr.socialNetwork.models.Phone;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PhoneDAO extends AbstractDAO<Phone> {
    private static final String TABLE = "phone";
    private static final String INSERT_VALUES = "NULL, ?";
    private static final String ID_TAG = "phone_id";
    private static final String UPDATE_VALUES = "phone_number = ?";

    private static final String JUNCTION_INSERT = "INSERT INTO account_phone VALUES(?, ?)";
    private static final String SELECT_ID_FROM_JUNCTION = "SELECT * FROM account_phone WHERE phone_id = ?";


    protected PhoneDAO() {
        super(TABLE, INSERT_VALUES, ID_TAG, UPDATE_VALUES, JUNCTION_INSERT);
    }

    private PhoneDAO(ConnectionPool pool) {
        this();
        this.pool = pool;
    }

    private static class InstanceHolder {
        private static final PhoneDAO INSTANCE = new PhoneDAO(JNDIConnectionPool.getInstance());
    }

    public static PhoneDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Phone entity) throws SQLException {
        statement.setString(1, entity.getPhoneNumber());
    }

    @Override
    protected void prepareJunctionInsert(PreparedStatement statement, Phone entity) throws DAOException, SQLException {
        statement.setLong(1, entity.getHolder().getId());
        statement.setLong(2, entity.getId());
    }

    @Override
    protected Phone readEntity(ResultSet results) throws DAOException, SQLException {
        Phone phone = new Phone();
        long phoneId = results.getLong(1);
        phone.setId(phoneId);
        long accountId = selectIdFromJunctionTable(SELECT_ID_FROM_JUNCTION, phoneId);
        Account holder = AccountDAO.getInstance().selectByID(accountId);
        phone.setHolder(holder);
        phone.setPhoneNumber(results.getString(2));
        return phone;
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Phone entity) throws SQLException {
        statement.setString(1, entity.getPhoneNumber());
        statement.setLong(2, entity.getId());
    }
}
