CREATE TABLE account (
	account_id BIGINT AUTO_INCREMENT,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	date_of_birth DATE NOT NULL,
	home_address VARCHAR(255),
	work_address VARCHAR(255),
	email VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,
	icq VARCHAR(20),
	skype VARCHAR(50),
	additional_info VARCHAR(255),
	registration_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (account_id)
);

CREATE TABLE phone (
	phone_id BIGINT AUTO_INCREMENT,
	phone_number VARCHAR(20) NOT NULL,
	PRIMARY KEY (phone_id)
);

CREATE TABLE account_phone (
	account_id BIGINT NOT NULL,
	phone_id BIGINT NOT NULL,
	FOREIGN KEY (account_id) REFERENCES account (account_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (phone_id) REFERENCES phone (phone_id) ON DELETE CASCADE ON UPDATE CASCADE,
	PRIMARY KEY (account_id, phone_id)
);

CREATE TABLE community (
	community_id BIGINT AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(255),
	founder_account_id BIGINT,
	date_founded TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (founder_account_id) REFERENCES account (account_id) ON UPDATE CASCADE ON DELETE SET NULL,
	PRIMARY KEY (community_id)
);

CREATE TABLE user_relationship (
	user_1_id BIGINT NOT NULL,
	user_2_id BIGINT NOT NULL,
	relationship_status TINYINT,
	acting_user_id BIGINT NOT NULL,
	FOREIGN KEY (user_1_id) REFERENCES account (account_id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (user_2_id) REFERENCES account (account_id) ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (user_1_id, user_2_id)
);