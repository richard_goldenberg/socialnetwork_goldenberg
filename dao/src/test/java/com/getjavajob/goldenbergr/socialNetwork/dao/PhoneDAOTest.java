package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.DataSourceHolder;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import com.getjavajob.goldenbergr.socialNetwork.models.Phone;
import org.h2.tools.RunScript;
import org.junit.*;

import java.io.InputStreamReader;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PhoneDAOTest extends DAOTest {

    private static PhoneDAO phoneDao;
    private static AccountDAO accountDAO;
    private Account holder;
    private Phone raw;

    @BeforeClass
    public static void init() throws SQLException {
        DataSourceHolder.setDataSource(createTestDataSource());
        phoneDao = PhoneDAO.getInstance();
        accountDAO = AccountDAO.getInstance();
    }

    @AfterClass
    public static void shutdown() {
        phoneDao = null;
        accountDAO = null;
    }

    @Before
    public void setUp() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/setup_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        holder = new Account("Foo", "Bar", null, null, null, "email@email.email", "mYpAsSword", null, null, null);
        holder.parseDateOfBirth("11-11-1971");
        accountDAO.insert(holder);
        raw = new Phone("111", holder);
    }

    @After
    public void tearDown() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/teardown_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        holder = null;
        raw = null;
    }

    @Test
    public void testInsertAndRead() throws DAOException {
        phoneDao.insert(raw);
        Phone pulled = phoneDao.selectByID(raw.getId());
        assertEquals(raw, pulled);
    }

    @Test
    public void testUpdate() throws DAOException {
        phoneDao.insert(raw);
        raw.setPhoneNumber("1337");
        phoneDao.update(raw);
        Phone pulled = phoneDao.selectByID(raw.getId());
        assertEquals(raw.getPhoneNumber(), pulled.getPhoneNumber());
        assertEquals(raw, pulled);
    }

    @Test
    public void testDelete() throws DAOException {
        phoneDao.insert(raw);
        phoneDao.deleteByID(raw.getId());
        assertNull(phoneDao.selectByID(raw.getId()));
    }
}