package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.DataSourceHolder;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import com.getjavajob.goldenbergr.socialNetwork.models.Community;
import org.h2.tools.RunScript;
import org.junit.*;

import java.io.InputStreamReader;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CommunityDAOTest extends DAOTest {


    private static CommunityDAO communityDao;
    private static AccountDAO accountDAO;
    private Account founder;
    private Community raw;


    @BeforeClass
    public static void init() throws SQLException {
        DataSourceHolder.setDataSource(createTestDataSource());
        communityDao = CommunityDAO.getInstance();
        accountDAO = AccountDAO.getInstance();
    }

    @AfterClass
    public static void shutdown() {
        communityDao = null;
        accountDAO = null;
    }

    @Before
    public void setUp() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/setup_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        founder = new Account("Foo", "Bar", null, null, null, "email@email.email", "mYpAsSword", null, null, null);
        founder.parseDateOfBirth("11-11-1971");
        accountDAO.insert(founder);
        raw = new Community("TestCommunity", null, founder);
    }

    @After
    public void tearDown() throws Exception {
        RunScript.execute(JDBCConnectionPool.getInstance().getConnection(), new InputStreamReader(ClassLoader.getSystemResourceAsStream("scripts/teardown_script.sql")));
        JDBCConnectionPool.getInstance().releaseConnection();
        founder = null;
        raw = null;
    }

    @Test
    public void testInsertAndRead() throws DAOException {
        communityDao.insert(raw);
        Community pulled = communityDao.selectByID(raw.getId());
        assertEquals(raw, pulled);
    }

    @Test
    public void testUpdate() throws DAOException {
        communityDao.insert(raw);
        String newName = "new community name";
        raw.setName(newName);
        communityDao.update(raw);
        Community pulled = communityDao.selectByID(raw.getId());
        assertEquals(raw.getName(), pulled.getName());
        assertEquals(raw, pulled);
    }

    @Test
    public void testDelete() throws DAOException {
        communityDao.insert(raw);
        communityDao.deleteByID(founder.getId());
        assertNull(communityDao.selectByID(founder.getId()));
    }
}