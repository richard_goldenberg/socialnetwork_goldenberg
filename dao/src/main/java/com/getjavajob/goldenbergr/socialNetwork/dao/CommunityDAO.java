package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.ConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.connections.JNDIConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;
import com.getjavajob.goldenbergr.socialNetwork.models.Community;

import java.sql.*;

public class CommunityDAO extends AbstractDAO<Community> {

    private static final String TABLE = "community";
    private static final String INSERT_VALUES = "NULL, ?, ?, ?, ?";
    private static final String ID_TAG = "community_id";
    private static final String UPDATE_VALUES = "name = ?, description = ?, founder_account_id = ?";

    protected CommunityDAO() {
        super(TABLE, INSERT_VALUES, ID_TAG, UPDATE_VALUES, null);
    }

    private CommunityDAO(ConnectionPool pool) {
        this();
        this.pool = pool;
    }

    private static class InstanceHolder {
        private static final CommunityDAO INSTANCE = new CommunityDAO(JNDIConnectionPool.getInstance());
    }

    public static CommunityDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Community entity) throws SQLException {
        setCommonValues(statement, entity);

        long currentTime = System.currentTimeMillis();
        java.util.Date currentDate = new Date(currentTime);
        Timestamp stamp = new Timestamp(currentTime);

        entity.setDateFounded(currentDate);
        statement.setTimestamp(4, stamp);
    }

    private void setCommonValues(PreparedStatement statement, Community entity) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getDescription());
        statement.setLong(3, entity.getFounder().getId());
    }

    @Override
    protected Community readEntity(ResultSet results) throws DAOException, SQLException {
        Community community = new Community();
        community.setId(results.getLong(1));
        community.setName(results.getString(2));
        community.setDescription(results.getString(3));
        Account founder = AccountDAO.getInstance().selectByID(results.getLong(4));
        community.setFounder(founder);
        community.setDateFounded(parseTimestamp(results.getTimestamp(5)));
        return community;
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Community entity) throws SQLException {
        setCommonValues(statement, entity);
        statement.setLong(4, entity.getId());
    }
}
