package com.getjavajob.goldenbergr.socialNetwork.connections;

import java.sql.Connection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class AbstractConnectionPool implements ConnectionPool {

    private static final int POOL_SIZE = 5;

    protected BlockingQueue<Connection> idleConnections = new LinkedBlockingQueue<>(POOL_SIZE);

    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();
    private ThreadLocal<Integer> activeRequests = new ThreadLocal<>();

    protected AbstractConnectionPool() {
        initializeConnections();
    }

    private void initializeConnections() {
        for (int i = 0; i < POOL_SIZE; i++) {
            idleConnections.add(createConnection());
        }
    }

    protected abstract Connection createConnection();

    @Override
    public Connection getConnection() {
        if (localConnection.get() != null) {
            return reuseAllocatedConnection();
        }
        return allocateConnection();
    }

    private Connection reuseAllocatedConnection() {
        activeRequests.set(activeRequests.get() + 1);
        return localConnection.get();
    }

    private Connection allocateConnection() {
        try {
            Connection connection = idleConnections.take();
            localConnection.set(connection);
            activeRequests.set(1);
            return connection;
        } catch (InterruptedException e) {
            throw new ConnectionPoolException("interrupted while waiting for connection", e);
        }
    }

    @Override
    public void releaseConnection() {
        activeRequests.set(activeRequests.get() - 1);
        if (activeRequests.get() == 0) {
            Connection returned = localConnection.get();
            localConnection.remove();
            idleConnections.add(returned);
        }
    }

    //testing

    int idleSize() {
        return idleConnections.size();
    }

    int activeRequest() {
        return activeRequests.get();
    }

    Connection getLocal() {
        return localConnection.get();
    }

    int maxConnections() {
        return POOL_SIZE;
    }
}
