package com.getjavajob.goldenbergr.socialNetwork.unfinished;

import com.getjavajob.goldenbergr.socialNetwork.connections.JDBCConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RelationshipDAO {

    private static final int PENDING = 1;
    private static final int ACCEPTED = 2;
    private static final int DECLINED = 3;
    private static final int BLOCKED = 4;

    private static final String INSERT_STATEMENT = "INSERT INTO user_relationship VALUES (?, ?, ?, ?)";
    private static final String SELECT_BY_ID = "SELECT * FROM user_relationship WHERE user_1_id = ? OR user_2_id = ?";
    private static final String UPDATE_STATEMENT = "UPDATE user_relationship SET relationship_status = ?, acting_user_id = ? WHERE user_1_id = ? AND user_2_id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM user_relationship WHERE user_1_id = ? AND user_2_id = ?";
    private static final String SELECT_ALL_FRIENDS = "SELECT * FROM user_relationship WHERE user_1_id = ? OR user_2_id = ?";

    private Connection connection;

    public void logFriendRequest(Account requestSender, Account requestReciever) {
        connection = JDBCConnectionPool.getInstance().getConnection();

        try (PreparedStatement ps = connection.prepareStatement(INSERT_STATEMENT)) {

        } catch (SQLException e) {

        }

        JDBCConnectionPool.getInstance().releaseConnection();
    }


}
