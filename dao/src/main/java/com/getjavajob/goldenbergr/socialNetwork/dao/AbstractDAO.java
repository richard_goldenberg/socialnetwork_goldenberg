package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.ConnectionPool;
import com.getjavajob.goldenbergr.socialNetwork.models.NetworkEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDAO<E extends NetworkEntity> implements DAO<E> {

    protected ConnectionPool pool;
    private Connection connection;

    private String insertStatement;
    private String selectById;
    private String updateStatement;
    private String deleteById;
    private String selectAll;

    private String junctionInsertStatement;

    //protected AbstractDAO() {}

    protected AbstractDAO(String table, String insertValues, String idTag,  String updateValues, String junctionInsertStatement) {
        initializeStatements(table, insertValues, idTag, updateValues);
        this.junctionInsertStatement = junctionInsertStatement;
    }

    private void initializeStatements(String table, String insertValues, String idTag, String updateValues) {
        insertStatement = "INSERT INTO " + table + " VALUES (" + insertValues + ")";
        selectById = "SELECT * FROM " + table + " WHERE " + idTag + "= ?";
        updateStatement = "UPDATE " + table + " SET " + updateValues + " WHERE " + idTag + "= ?";
        deleteById = "DELETE FROM " + table + " WHERE " + idTag + "= ?";
        selectAll = "SELECT * FROM " + table;
    }

    @Override
    public long insert(E entity) throws DAOException {
        acquireConnection();
        long id = executeInsertion(entity);
        updateJunctionTable(entity, junctionInsertStatement);
        commitAndReleaseConnection();
        return id;
    }

    private long executeInsertion(E entity) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS)) {
            prepareInsertStatement(statement, entity);
            return executeAndProcessId(statement, entity);
        } catch (SQLException e) {
            throw new DAOException("insertion failed, doing rollback", e);
        } finally {
            rollback();
        }
    }

    protected abstract void prepareInsertStatement(PreparedStatement statement, E entity) throws SQLException;

    private long executeAndProcessId(PreparedStatement statement, E entity) throws DAOException, SQLException {
        statement.executeUpdate();
        try(ResultSet result = statement.getGeneratedKeys()) {
            if (result.next()) {
                long id =  result.getLong(1);
                entity.setId(id);
                connection.commit();
                return id;
            } else {
                throw new DAOException("insertion failed, no rows affected");
            }
        }
    }

    /**
     *
     * Methods for updating and querying the junction table associated with the specified database entity
     *
     * */

    protected long selectIdFromJunctionTable(String selectStatement, long target) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(selectStatement)) {
            statement.setLong(1, target);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    return results.getLong(1);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("id select failed", e);
        }
        throw new DAOException("no such entry exists in junction table");
    }

    private void updateJunctionTable(E entity, String junctionInsertStatement) throws DAOException {
        if (junctionInsertStatement == null) {
            return;
        }
        try(PreparedStatement statement = connection.prepareStatement(junctionInsertStatement)) {
            prepareJunctionInsert(statement, entity);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new DAOException("junction insertion failed", e);
        } finally {
            rollback();
        }
    }

    /**
     * IMPORTANT
     * This method must be overridden in a subclass if junction table use is intended
     *
     */

    protected void prepareJunctionInsert(PreparedStatement statement, E entity) throws DAOException, SQLException {
        throw new DAOException("unexpected method call, junction table not specified for this object");
    }

    @Override
    public E selectByID(long id) throws DAOException {
        acquireConnection();
        E entity = executeSelectQuery(id);
        commitAndReleaseConnection();
        return entity;
    }

    private E executeSelectQuery(long id) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(selectById)) {
            statement.setLong(1, id);
            return executeAndProcessResults(statement);
        } catch (SQLException e) {
            throw new DAOException("select failed", e);
        } finally {
            rollback();
        }
    }

    private E executeAndProcessResults(PreparedStatement statement) throws SQLException, DAOException {
        try(ResultSet results = statement.executeQuery()) {
            E entity = null;
            if (results.next()) {
                entity = readEntity(results);
            }
            connection.commit();
            return entity;
        }
    }

    protected abstract E readEntity(ResultSet results) throws DAOException, SQLException;

    @Override
    public boolean update(E entity) throws DAOException {
        acquireConnection();
        boolean updated = executeUpdate(entity);
        commitAndReleaseConnection();
        return updated;
    }

    private boolean executeUpdate(E entity) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(updateStatement)) {
            prepareUpdateStatement(statement, entity);
            int flag = statement.executeUpdate();
            connection.commit();
            return flag > 0;
        } catch (SQLException e) {
            throw new DAOException("update failed", e);
        } finally {
            rollback();
        }
    }

    protected abstract void prepareUpdateStatement(PreparedStatement statement, E entity) throws SQLException;

    @Override
    public boolean deleteByID(long id) throws DAOException {
        acquireConnection();
        boolean deleted = delete(id);
        commitAndReleaseConnection();
        return deleted;
    }

    private boolean delete(long id) throws DAOException{
        try(PreparedStatement statement = connection.prepareStatement(deleteById)) {
            statement.setLong(1, id);
            int flag = statement.executeUpdate();
            connection.commit();
            return flag > 0;
        } catch (SQLException e) {
            throw new DAOException("deletion failed", e);
        } finally {
            rollback();
        }
    }

    @Override
    public List<E> selectAll() throws DAOException {
        acquireConnection();
        List<E> entities = readAll("");
        commitAndReleaseConnection();
        return entities;
    }

    protected List<E> readAll(String where) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(selectAll + where)) {
            return readResults(statement);
        } catch (SQLException e) {
            throw new DAOException("selection failed", e);
        } finally {
            rollback();
        }
    }

    private List<E> readResults(PreparedStatement statement) throws SQLException, DAOException {
        try(ResultSet results = statement.executeQuery()) {
            List<E> entities = new ArrayList<>();
            while (results.next()) {
                E entity = readEntity(results);
                entities.add(entity);
            }
            connection.commit();
            return entities;
        }
    }

    private void acquireConnection() {
        connection = pool.getConnection();
    }

    private void rollback() throws DAOException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DAOException("rollback failed", e);
        }
    }

    private void commitAndReleaseConnection() throws DAOException {
        try {
            connection.commit();
            connection = null;
        } catch (SQLException e) {
            throw new DAOException("commit failed, releasing connection");
        } finally {
            pool.releaseConnection();
        }
    }

    // SQL <--> JAVA data conversion utility methods

    static java.util.Date parseTimestamp(Timestamp stamp) {
        return new java.util.Date(stamp.getTime());
    }

    static java.sql.Date parseFromDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    static java.util.Date parseToDate(java.sql.Date date) {
        return new java.util.Date(date.getTime());
    }
}
