package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.connections.ConnectionPoolException;
import com.getjavajob.goldenbergr.socialNetwork.connections.DataSourceHolder;
import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DAOTest {

    protected static DataSource createTestDataSource() {
        Properties props = new Properties();
        try (InputStream is = DataSourceHolder.class.getClassLoader().getResourceAsStream("dbConnection.properties")) {
            props.load(is);
        } catch (IOException e) {
            throw new ConnectionPoolException("processing properties failed", e);
        }
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(props.getProperty("database.driver"));
        ds.setUrl(props.getProperty("database.url"));
        ds.setUsername(props.getProperty("database.user"));
        ds.setPassword(props.getProperty("database.password"));
        return ds;
    }
}
