package com.getjavajob.goldenbergr.socialNetwork.connections;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.*;

public class JDBCConnectionPoolTest {

    private JDBCConnectionPool pool;
    private CyclicBarrier barrier;

    @Before
    public void setUp() throws Exception {
        pool = JDBCConnectionPool.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        barrier = null;
        pool.reset();
    }

    @Test
    public void sameRealConnection() {
        Connection connectionOne = pool.getConnection();
        assertNotNull(connectionOne);
        Connection connectionTwo = pool.getConnection();
        assertSame(connectionOne, connectionTwo);
    }

    @Test
    public void testUniqueness() throws BrokenBarrierException, InterruptedException {
        Set<Connection> connections = Collections.synchronizedSet(new HashSet<Connection>());
        barrier = new CyclicBarrier(pool.maxConnections() + 1);
        for (int i = 0; i < pool.maxConnections(); i++) {
            new Thread(new UniquenessTester(connections, barrier)).start();
        }

        //waiting for threads to get their connections and add them to set
        barrier.await();
        assertEquals(connections.size(), pool.maxConnections());
    }

    @Test(timeout = 1000)
    public void testBlocking() throws BrokenBarrierException, InterruptedException {
        barrier = new CyclicBarrier(pool.maxConnections() + 1);
        for (int i = 0; i < pool.maxConnections(); i++) {
            new Thread(new ExtensionTester(barrier)).start();
        }
        barrier.await();
        Lock terminationLock = new ReentrantLock();
        Thread toBeBlocked = new Thread(new BlockingTester(terminationLock));
        toBeBlocked.start();
        while (toBeBlocked.getState() != Thread.State.WAITING) {}
        assertEquals(Thread.State.WAITING, toBeBlocked.getState());
        barrier.await();
        barrier.await();
        terminationLock.lock();
        assertEquals(Thread.State.TERMINATED, toBeBlocked.getState());
    }

    private static class BlockingTester implements Runnable {

        private Lock terminationLock;

        BlockingTester(Lock lock) {
            terminationLock = lock;
        }

        @Override
        public void run() {
            terminationLock.lock();
            JDBCConnectionPool.getInstance().getConnection();
            JDBCConnectionPool.getInstance().releaseConnection();
            terminationLock.unlock();
        }
    }

    private static class UniquenessTester implements Runnable {
        private Set<Connection> set;
        private CyclicBarrier barrier;

        UniquenessTester(Set<Connection> set, CyclicBarrier barrier) {
            this.set = set;
            this.barrier = barrier;
        }

        @Override
        public void run() {
            try {
                set.add(JDBCConnectionPool.getInstance().getConnection());
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    private static class ExtensionTester implements Runnable {

        private CyclicBarrier barrier;

        ExtensionTester(CyclicBarrier barrier) {
            this.barrier = barrier;
        }

        @Override
        public void run() {
            try {
                JDBCConnectionPool.getInstance().getConnection();
                //blocks after receiving connection
                barrier.await();
                //blocks for main thread to perform assertion
                barrier.await();
                JDBCConnectionPool.getInstance().releaseConnection();
                //blocks to signify connection release
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}