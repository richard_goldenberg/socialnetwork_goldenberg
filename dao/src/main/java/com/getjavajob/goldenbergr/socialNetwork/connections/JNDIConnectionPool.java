package com.getjavajob.goldenbergr.socialNetwork.connections;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JNDIConnectionPool extends AbstractConnectionPool {

    private static class InstanceHolder {
        private static JNDIConnectionPool INSTANCE = new JNDIConnectionPool();
    }

    private DataSource dataSource;

    private JNDIConnectionPool() {}

    public static JNDIConnectionPool getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected Connection createConnection() {
        if (dataSource == null) {
            init();
        }
        Connection connection;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new ConnectionPoolException("couldn't get connection from DataSource", e);
        }
    }

    private void init() {
        dataSource = DataSourceHolder.getDataSource();
    }
}
