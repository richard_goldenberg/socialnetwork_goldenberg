package com.getjavajob.goldenbergr.socialNetwork.unfinished;

public enum FriendRequestStatus {
    PENDING(1), ACCEPTED(2), DECLINED(3), BLOCKED(4);

    private int status;

    FriendRequestStatus(int status) {
        this.status = status;
    }
}
