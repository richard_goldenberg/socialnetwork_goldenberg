package com.getjavajob.goldenbergr.socialNetwork.connections;

import javax.sql.DataSource;

public class DataSourceHolder {

    private static DataSource dataSource;

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static void setDataSource(DataSource dataSource) {
        DataSourceHolder.dataSource = dataSource;
    }
}
