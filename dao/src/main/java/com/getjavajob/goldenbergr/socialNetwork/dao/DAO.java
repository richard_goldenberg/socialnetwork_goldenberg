package com.getjavajob.goldenbergr.socialNetwork.dao;

import com.getjavajob.goldenbergr.socialNetwork.models.NetworkEntity;

import java.util.List;

public interface DAO<E extends NetworkEntity> {

    long insert(E entity) throws DAOException;

    E selectByID(long id) throws DAOException;

    boolean update(E entity) throws DAOException;

    boolean deleteByID(long id) throws DAOException;

    List<E> selectAll() throws DAOException;

}
