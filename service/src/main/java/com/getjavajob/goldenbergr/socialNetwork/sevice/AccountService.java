package com.getjavajob.goldenbergr.socialNetwork.sevice;

import com.getjavajob.goldenbergr.socialNetwork.dao.AccountDAO;
import com.getjavajob.goldenbergr.socialNetwork.dao.DAOException;
import com.getjavajob.goldenbergr.socialNetwork.models.Account;

import java.util.List;

public class AccountService {

    private AccountDAO aDao = AccountDAO.getInstance();

    public void createAccount(Account account) throws ServiceException {
        try {
            aDao.insert(account);
        } catch (DAOException e) {
            throw new ServiceException("account creation failed", e);
        }
    }

    public void updateAccount(Account account) throws ServiceException {
        try {
            aDao.update(account);
        } catch (DAOException e) {
            throw new ServiceException("account update failed", e);
        }
    }

    public void deleteAccount(Account account) throws ServiceException {
        try {
            aDao.deleteByID(account.getId());
        } catch (DAOException e) {
            throw new ServiceException("account deletion failed", e);
        }
    }

    public List<Account> getAll() throws ServiceException {
        try {
            return aDao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("account list retrieval failed", e);
        }
    }
}
