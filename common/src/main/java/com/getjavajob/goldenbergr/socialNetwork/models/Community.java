package com.getjavajob.goldenbergr.socialNetwork.models;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Community extends NetworkEntity {

    private String name;
    private String description;
    private Account founder;
    private Date dateFounded;

    private List<Account> groupMembers;
    private List<Account> groupModerators;

    public Community() {}

    public Community(String name, String description, Account founder) {
        this.name = name;
        this.description = description;
        this.founder = founder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Account getFounder() {
        return founder;
    }

    public void setFounder(Account founder) {
        this.founder = founder;
    }

    public void setDateFounded(Date date) {
        this.dateFounded = date;
    }

    public Date getDateFounded() {
        return dateFounded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Community community = (Community) o;
        return id == community.id &&
                Objects.equals(name, community.name) &&
                Objects.equals(description, community.description) &&
                Objects.equals(founder, community.founder) &&
                Objects.equals(dateFounded, community.dateFounded);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, founder, dateFounded);
    }

    @Override
    public String toString() {
        return "Community{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", founder=" + founder +
                ", dateFounded=" + dateFounded +
                '}';
    }
}
